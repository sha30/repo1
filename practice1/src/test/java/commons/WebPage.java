package commons;
/*
* this page is totally for the driver related suff
* this class created by prathyusha on 12/24/2020
* */
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class WebPage {



    public WebDriver webdriver;

    public WebPage(){}

    public WebPage (WebDriver webdriver)
    {
        this.webdriver = webdriver;
    }

    public WebDriver getWebDriver(String browserName)
    {
       switch (browserName)
       {
        case "chrome" :

            System.setProperty("webdriver.chrome.driver", "C:/SAutomation/repo1/practice1/drivers/chromedriver.exe");
            ChromeOptions ops = new ChromeOptions();
            ops.addArguments("start-minimized");
            ops.addArguments("--incognito");
            ops.addArguments("window-size=1400,800");
            webdriver = new ChromeDriver(ops);
            break;


           default:
               throw new IllegalStateException("Unexpected value: " + browserName);
       }
        return webdriver;
    }


}
