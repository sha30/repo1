package pages;

import commons.WebPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class CallingDriver {
    WebDriver d;
    @Test
    public void callDriver(){
        WebPage bp = new WebPage();
        d = bp.getWebDriver("chrome");
        d.get("https://www.facebook.com");
    }
}
